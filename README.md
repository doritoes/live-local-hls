# Live Local HLS
A docker container to run a rtmp server with a HLS javascript front end for 
live local video streaming. This project is named "Live Local HLS" (llhls) for
its use of HLS video to deliver live, local video streams. This is not the same
as Apple's LLHS (low latency HLS).

This project brings together a quick rtmp streaming server such that the stream
can be viewed in a browser over the local wireless LAN (WLAN) without an app.

Components:
*  https://hub.docker.com/r/alfg/nginx-rtmp/ (nginx rtmp server)
*  https://github.com/video-dev/hls.js/

[![Docker Pulls](https://img.shields.io/docker/pulls/doritoes/llhls.svg)](https://hub.docker.com/r/doritoes/llhls/)

# Purpose
Sometimes you just need a quick local stream. You have OBS running on a PC with
a video capture device. If you have a BlackMagic Web Presenter you can use this
with a completely Linux computer. Less expensive capture cards like the
El Gato HD60 S require Windows or Mac, so you might be running any of these
operating systems.

Many times your users don't have VLC or another streaming client installed on
their device. So HLS is ideal to deliver a stream right inside the user's
browser, mobile or PC

This project brings together pre-existing work to allow standing up a quick
media streaming server on the same OBS commputer, or on a different system
as designed.

It is intended to be very rough, requiring you to customize the art and text.

# Quick Start
Requirements:
* Docker installed

Start Up:
*  docker pull doritoes/llhls
*  docker run -d --name stream -p 80:80 -p 1935:1935 --restart=always doritoes/llhls
*  docker ps
*  docker exec -it stream /bin/sh
*  Browse to http://localhost
*  Point OBS to rtmp://localhost:1935/stream with stream key **program**
*  If you are accessing from a different system, use the IP address not localhost
*  If you didn't set the --name to stream, **use docker ps** and use the container ID

Stopping, Removing, and Pausing:
*  docker stop stream
*  docker rm stream 
*  docker pause stream
*  docker unpause stream

Other Userful Docker Commands:
*  docker restart stream
*  docker kill stream
*  docker rmi doritoes/llhls
*  docker exec stream nginx -s reload

# Rebuilding without the Image
*  Clone this repository to get the files on your computer
   *  git clone https://gitlab.com/doritoes/live-local-hls.git
*  Customize the html and image files
   *   if you add other text files other than .html, you will need add commands to the dockerfile to run dos2unix on them
*  Build the image  
   * cd live-local-hls
   * docker build -t stream .
*  Start the image
   *  docker run -d --name stream -p 80:80 -p 1935:1935 --restart=always stream
