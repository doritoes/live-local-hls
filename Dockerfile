FROM alfg/nginx-rtmp

MAINTAINER doritoes <seth.holcomb@gmail.com>

# Install package curl, nginx config, web files; upgrade packages
COPY --chmod=777 nginx.conf /etc/nginx/nginx.conf.template
COPY html/ /usr/local/nginx/html/
RUN apk add --no-cache curl \
    && apk update \
    && apk upgrade \
    && curl -O /usr/local/nginx/html/script/hls.js \
       https://cdn.jsdelivr.net/npm/hls.js@latest \
    && apk del curl \
    && dos2unix /etc/nginx/nginx.conf.template \
    && dos2unix /usr/local/nginx/html/script/hls.js \
    && find /usr/local/nginx/html -type f -name \*html -print0 | xargs -0 dos2unix --
